A simple chat client/server system currently implemented in Python.

Use:

Run the server: python/chat_server localhost 9999

Run client(s): python/chat_client localhost	9999 10000 username

Type your messages.