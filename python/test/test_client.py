
import threading
import sys

if sys.version_info < (2, 7):
	import unittest2 as unittest
else:
	import unittest

from chat.back.server import ChatServer
from chat.back.client import ChatClient


class TestClient(unittest.TestCase):

	HOST = "localhost"
	PORT = 10000

	def setUp(self):
		# set a server up to test on
		server = ChatServer((self.HOST, self.PORT))
		self.server = server

		def start_server(): server.serve_forever()
		t = threading.Thread(target=start_server)
		t.daemon = True
		t.start()

		# set up a client to use
		self.client = ChatClient()
		self.client.connect((self.HOST, self.PORT), username="Halogenic")


	def test_send_message(self):
		self.client.send_message("bob", "hello there")


	def tearDown(self):
		self.client.disconnect()

		self.server.shutdown()