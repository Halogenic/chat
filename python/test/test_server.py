
import socket
import sys
import threading

# make compatible with python2.6
if sys.version_info < (2, 7):
	import unittest2 as unittest
else:
	import unittest

from chat.back.server import ChatServer


class TestServer(unittest.TestCase):

	HOST = "localhost"
	PORT = 9999

	def setUp(self):
		server = ChatServer((self.HOST, self.PORT))
		self.server = server

		def start_server(): server.serve_forever()
		t = threading.Thread(target=start_server)
		t.daemon = True
		t.start()

	def test_server_is_connectable(self):
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		try:
			sock.connect((self.HOST, self.PORT))
		except socket.error:
			self.fail("socket failed to connect to server on %s and %s" % (self.HOST, self.PORT))

	def tearDown(self):
		self.server.shutdown()