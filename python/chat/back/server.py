
import json
import socket
import SocketServer
import threading

from chat.back.util import normalize_username


USER_DATABASE_LOCATION = ""


class ServerError(Exception):
	pass


class User(object):
	"""
	Encapsulates sending messages to the user and stores
	user data.
	"""

	def send_message(self, data):
		self.sock.sendall()


class UserManager(object):

	def __init__(self):
		self._users = {}
		self._address = {}

		self._lock = threading.Lock()


	def add(self, username, address, server_address):
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		try:
			sock.connect(server_address)
		except socket.error as e:
			raise ServerError("Could not connect to client's message server\n%s" % e)

		with self._lock:
			self._users[username] = {
				"address": address,
				"server_address": server_address,
				"socket": sock, # the socket connection to this client's server
				"contacts": []
			}

			self._address[address] = username



	def remove(self, username):
		address = self._users[username]["address"]

		# close the socket
		self._users[username]["socket"].close()

		with self._lock:
			del self._users[username]
			del self._address[address]


	def get(self, username):
		return self._users[username]


	def get_username(self, address):
		print self._address

		return self._address[address]


	def get_all_usernames(self):
		return self._users.keys()


	def __iter__(self):
		return iter(self._users.values())


class ChatServerTCPHandler(SocketServer.StreamRequestHandler):

	def setup(self):
		SocketServer.StreamRequestHandler.setup(self)

		self.user_manager = self.server.user_manager

	
	def handle(self):
		print "Client at %s connected" % str(self.client_address)

		# handle the connection indefinitely or until 
		while True:
			data = self.rfile.readline().strip()

			if data:
				self.decode_client_message(self.decode(data))


	def decode_client_message(self, message):
		# TODO: have an option to decrypt the payload if needed

		data = message["data"]
		data["sender_address"] = self.client_address

		{ 
			"connect": self.register_user,
			"disconnect": self.unregister_user,
			"send_message": self.send_message,
			"get_contacts": self.get_contacts
		}.get(message["type"], lambda d: None)(data)


	def register_user(self, data):
		username = normalize_username(data["username"])

		# TODO:
		# check user in registered database, if they do not
		# not exist then send an error response

		# TODO: check if this user is already registered
		# and send error response if they are

		self.user_manager.add(username, self.client_address, tuple(data["server_address"]))

		print "Registered: %s at %s" % (username, self.client_address)

		# TODO: send a 'user connected' message to other connected users
		# on the user's contact list or all of them

		encoded_data = "%s\n" % json.dumps({
			"type": "connected",
			"data": {
				"username": username
			}
		})

		# for user in self.user_manager:
		# 	user["socket"].sendall(encoded_data)


	def unregister_user(self, data):
		# username = normalize_username(self._address_users[data["sender_address"]])
		username = normalize_username(data["username"])

		self.user_manager.remove(username)

		print "Unregistered: %s at %s" % (username, self.client_address)

		# TODO: send a 'user disconnected' message to other connected users


	def get_contacts(self, data):
		"Find and send the contacts for the sender to the sender"

		# at the moment just send all connected users
		sender = normalize_username(data["sender"])

		user = self.user_manager.get(sender)

		contacts = self.user_manager.get_all_usernames()
		contacts.remove(sender)

		encoded_data = self.encode({
			"type": "contacts_list",
			"data": {
				"contacts": contacts
			}
		})

		user["socket"].sendall(encoded_data)


	def send_message(self, data):
		sender = normalize_username(data["sender"])
		receivers = data["receivers"]

		encoded_data = self.encode({
			"type": "message",
			"data": {
				"sender": sender,
				"content": data["content"]
			}
		})

		print "Sending message: %s at %s to [%s]" % (sender, self.client_address, ", ".join(receivers))

		errors = []

		for receiver in receivers:
			try:
				user = self.user_manager.get(receiver)
			except KeyError:
				errors.append("User not reachable: %s" % receiver)
				continue

			user["socket"].sendall(encoded_data)

		# send any errors to the sender
		if errors:
			sender_user = self.user_manager.get(sender)

			error_message = {
				"type": "error",
				"data": {
					"reason": "\n".join(errors)
				}
			}

			sender_user["socket"].sendall("%s\n" % json.dumps(error_message))

			print "Sent errors: %s at %s of [%s]" % (sender, self.client_address, error_message["data"]["reason"])


	def encode(self, data):
		return "%s\n" % json.dumps(data)


	def decode(self, data):
		return json.loads(data)


# could also use ForkingMixIn for processes
class ChatServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
# class ChatServer(SocketServer.TCPServer):
	"""
	The ChatServer works as a master server, able to respond to a number
	of different messages sent by the client to mediate chat sessions and
	give users listings of who is online.

	User names are case insensitive and will make all attempts to correctly
	support unicode when normalizing a username.

	Protocol:
		The low-level communication protocol to the server using sockets
		expects a json dict as one line for each command, so when
		sending the data use "%s\n" % json.dumps(data)

		These messages are dicts of the form:
		{
			"type": "message type",
			"data": {} # data associated with the message as outlined below
		}

		# an error message sent to the client (or in some cases the server if needed)
		error: {
			"reason": "user is not connected"
		}

		# connects a client to the master server using their
		# user name, if this user name is not registered an 
		# error response will be returned. If it is registered
		# the user's IP Address will then become associated with
		# that user in the Server.
		connect: {
			"username": "halogenic"
		}

		disconnect: {
			"username": "halogenic"
		}

		# sends a chat message to the specified user
		send_message: {
			"target": "someone else",
			"content": "Hello there"
		}
	"""

	allow_reuse_address = True
	daemon_threads = True

	def __init__(self, address):
		SocketServer.TCPServer.__init__(self, address, ChatServerTCPHandler)

		self.user_manager = UserManager()

