
import json
import socket
import SocketServer
import threading


class ClientError(Exception):
	pass


class ChatClientTCPHandler(SocketServer.StreamRequestHandler):

	def handle(self):
		while True:
			data = self.rfile.readline().strip()

			if data:
				self.decode_server_message(json.loads(data))


	def decode_server_message(self, message):
		data = message["data"]

		{
			"error": self.handle_server_error,
			"message": self.receive_message,
			"contacts_list": self.contacts_list
		}.get(message["type"], lambda d: None)(data)


	def handle_server_error(self, data):
		print "Server error: %s" % data["reason"]


	def receive_message(self, data):
		print "%s: %s" % (data["sender"], data["content"])


	def contacts_list(self, data):
		contacts = data["contacts"]

		print contacts


class ChatClientMessageServer(SocketServer.TCPServer):
	"""
	Allows messages from the master server to be sent to the client, such 
	as errors or chat messages sent by other users.
	"""

	allow_reuse_address = True
	# daemon_threads = True

	def __init__(self, address):
		SocketServer.TCPServer.__init__(self, address, ChatClientTCPHandler)


class ChatClient(object):

	def __init__(self, username, message_server_address):
		self.__sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		self.__username = username

		self.__server = ChatClientMessageServer(message_server_address)

		self.__server_thread = None


	@property
	def username(self):
		"The current session's username"
		return self.__username


	def connect(self, address):
		"Connect to server at 'address'"

		# start our message server inside a thread for asynchronous comms between client and master server
		self.__server_thread = threading.Thread(target=self.__server.serve_forever)
		self.__server_thread.daemon = True
		self.__server_thread.start()
		
		try:
			self.__sock.connect(address)
		except socket.error:
			raise ClientError("Could not connect to server at %s" % str(address))

		# once connected to the server we need to register our username
		self._send({ "type": "connect", "data": { "username": self.__username, "server_address": self.__server.server_address } })

		# now get the contacts list
		self._send({ "type": "get_contacts", "data": { "sender": self.__username } })


	def disconnect(self):
		"Disconnect from the active server"

		# send the disconnect message to the server
		self._send({ "type": "disconnect", "data": { "username": self.__username }})

		self.__sock.close()

		# stop our message server, this will also exit the server thread
		self.__server.shutdown()


	def send_message(self, receivers, content):
		"Send a message to the clients specified by 'receivers'"

		self._send({ "type": "send_message", "data": { "sender": self.__username, "receivers": receivers, "content": content } })


	def add_server_message_handler(self, func):
		"""
		Install a callback function that gets called when a message
		is received from the server, allowing the calling program 
		to customise what action(s) to take. This allows support of 
		different types of front end interface as the callbacks
		can handle messages in different ways.
		"""

		pass


	def _send(self, data):
		# TODO: have an option to encrypt the payload

		try:
			self.__sock.sendall("%s\n" % json.dumps(data))
		except socket.error as e:
			raise ClientError("Could not send data to server\n%s" % e)

